import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ProductService} from "../../shared/product.service";
import {Router} from "@angular/router";


@Component({
  selector: 'app-add-page',
  templateUrl: './add-page.component.html',
  styles: [
  ]
})
export class AddPageComponent implements OnInit {

  form: FormGroup;
  submited = false;

  constructor(private productService: ProductService, private router: Router) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      type: new FormControl(null, [Validators.required]),
      title: new FormControl(null, [Validators.required]),
      photo: new FormControl(null, [Validators.required]),
      info: new FormControl(null, [Validators.required]),
      price: new FormControl(null, [Validators.required])
    })
  }

  onSubmit() {
    if(this.form.invalid) {
      return;
    }

    this.submited = true;

    const product = {
     type: this.form.value.type,
     title: this.form.value.title,
     photo: this.form.value.photo,
     info: this.form.value.info,
     price: this.form.value.price,
     date: new Date()
    }

    this.productService.addProduct(product).subscribe( result => {
        this.form.reset();
        this.submited = false;
        this.router.navigate(['/']);
    });
  }
}
