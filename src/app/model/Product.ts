export class Product {

  id: number;
  type: string;
  title: string;
  photo?: string;
  info?: string;
  price?: number;
  name?: string;

  constructor(type: string, title: string, photo?: string, info?: string, price?: number, name?: string, id?: number) {

    this.type = type;
    this.title = title;
    this.photo = photo;
    this.info = info;
    this.price = price;
    this.name = name;
    this.id = id;
  }
}
