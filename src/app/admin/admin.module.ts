import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { AdminRoutingModule } from "./admin-routing.module";
import { AdminLayoutComponent } from './shured/admin-layout/admin-layout.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { AddPageComponent } from './add-page/add-page.component';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { EditPageComponent } from './edit-page/edit-page.component';
import { OrdersPageComponent } from './orders-page/orders-page.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { QuillModule } from 'ngx-quill';
import {SearchPipe} from "../shared/search.pipe";


@NgModule({
  declarations: [AdminLayoutComponent,
    LoginPageComponent,
    AddPageComponent,
    DashboardPageComponent,
    EditPageComponent,
    OrdersPageComponent,
    SearchPipe
  ],
  imports: [CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    QuillModule.forRoot()],
  exports: [
    LoginPageComponent,
    DashboardPageComponent
  ]
})

export class AdminModule {}
