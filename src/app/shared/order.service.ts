import { Injectable } from '@angular/core';
import {Order, Product} from "../admin/shured/FbResponse";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  type = 'Phone'
  cardProduct: Product[] = []

  constructor(private http: HttpClient) { }

  create(order: Order) {
    return this.http.post(`${environment.fbDbUrl}/orders.json`, order)
      .pipe(map((res : Order) => {
        return {
          ...order,
          id: res.name,
          date: new Date(order.date)
        }
      }));
  }

  getAll() {
    return this.http.get(`${environment.fbDbUrl}/orders.json`)
      .pipe(map(result => {
        return Object.keys(result)
          .map(key => ({
            ...result[key],
            id: key,
            date: new Date(result[key].date)
          }))
      }))
  }

  remove(id: number) {
    return this.http.delete(`${environment.fbDbUrl}/orders/${id}.json`);
  }

  /*getById(id: number) {
    return this.http.get(`${environment.fbDbUrl}/products/${id}.json`)
      .pipe(map((result: Product) => {
        return {
          ...result,
          id,
          date: new Date(result.date)
        }
      }))
  }

  update(product: Product) {
    return this.http.patch(`${environment.fbDbUrl}/products/${product.id}.json`, product);
  }*/

}
