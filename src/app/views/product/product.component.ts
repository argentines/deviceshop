import {Component, Input, OnInit} from '@angular/core';
import {ProductService} from "../../shared/product.service";
import {Product} from "../../admin/shured/FbResponse";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  @Input()
  products: Product[];

  constructor(public productService: ProductService) {}

  ngOnInit(): void {
  }

  addProduct(product: Product) {
    this.productService.addProductTuCard(product)
  }
}
