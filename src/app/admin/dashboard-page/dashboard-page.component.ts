import { Component, OnInit } from '@angular/core';
import {Subscription} from "rxjs";
import {ProductService} from "../../shared/product.service";
import {Product} from "../shured/FbResponse";

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styles: [
  ]
})
export class DashboardPageComponent implements OnInit {

  products: Product[] = [];
  pSub: Subscription;
  rSub: Subscription;
  productName;

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.pSub = this.productService.getAll().subscribe(products => {
      this.products = products;
    });
  }

  ngOnDestroy() {
    if(this.pSub) {
      this.pSub.unsubscribe();
    }

    if(this.rSub) {
      this.rSub.unsubscribe();
    }
  }

  remove(id: number) {
    this.rSub = this.productService.remove(id).subscribe(() => {
      this.products = this.products.filter(product => product.id !== id);
    })
  }
}
