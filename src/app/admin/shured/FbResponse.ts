export interface FbResponse {
  name: string
}

export interface Product {

  id?: number;
  type: string;
  title: string;
  photo: string;
  info: string;
  price: number;
  name: string;
  date?: Date;
}

export interface Order {

  id?: number;
  name: string;
  phone: string,
  address: string,
  orders: Object,
  payment: number,
  price: number,
  date?: Date

}
