import { Component, OnInit } from '@angular/core';
import {Product} from "../../admin/shured/FbResponse";
import {ActivatedRoute} from "@angular/router";
import {ProductService} from "../../shared/product.service";
import {switchMap} from "rxjs/operators";

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.scss']
})
export class ProductPageComponent implements OnInit {

  product$;

  constructor(private route: ActivatedRoute, private productService: ProductService) { }

  ngOnInit(): void {
    this.product$ = this.route.params
      .pipe(switchMap(params => {
        return this.productService.getById(params['id']);
      }))
  }

  addProduct(product: Product) {
    this.productService.addProductTuCard(product)
  }
}
