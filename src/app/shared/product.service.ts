import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

import {map} from "rxjs/operators";
import {environment} from "../../environments/environment";
import {Product} from "../admin/shured/FbResponse";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  type = 'Phone'
  cardProduct: Product[] = []

  constructor(private http: HttpClient) { }

  addProduct(product) {
    return this.http.post(`${environment.fbDbUrl}/products.json`, product)
     .pipe(map((res : Product) => {
      return {
        ...product,
        id: res.name,
        date: new Date(product.date)
      }
     }));
  }

  getAll() {
    return this.http.get(`${environment.fbDbUrl}/products.json`)
      .pipe(map(result => {
            return Object.keys(result)
                         .map(key => ({
                           ...result[key],
                             id: key,
                             date: new Date(result[key].date)
                         }))
      }))
  }

  getById(id: number) {
    return this.http.get(`${environment.fbDbUrl}/products/${id}.json`)
      .pipe(map((result: Product) => {
        return {
            ...result,
            id,
            date: new Date(result.date)
          }
      }))
  }

  update(product: Product) {
    return this.http.patch(`${environment.fbDbUrl}/products/${product.id}.json`, product);
  }

  remove(id: number) {
    return this.http.delete(`${environment.fbDbUrl}/products/${id}.json`);
  }

  setType(type: string) {
    this.type = type;
  }

  addProductTuCard(product: Product) {
    this.cardProduct.push(product)
  }
}
