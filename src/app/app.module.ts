import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { MainLayoutComponent } from './shared/main-layout/main-layout.component';
import { MainPageComponent } from './views/main-page/main-page.component';
import { ProductPageComponent } from './views/product-page/product-page.component';
import { CartPageComponent } from './views/cart-page/cart-page.component';
import { HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { QuillModule } from "ngx-quill";
import {AuthInterceptor} from "./shared/auth-interceptor";
import { ProductComponent } from './views/product/product.component';
import { SortingPipe } from './shared/sorting.pipe';
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
    declarations: [
        AppComponent,
        MainLayoutComponent,
        MainPageComponent,
        ProductPageComponent,
        CartPageComponent,
        ProductComponent,
        SortingPipe
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        QuillModule.forRoot(),
        ReactiveFormsModule

    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            multi: true,
            useClass: AuthInterceptor,
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
