import { Component, OnInit } from '@angular/core';
import {Product} from "../../admin/shured/FbResponse";
import {ProductService} from "../../shared/product.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {OrderService} from "../../shared/order.service";

@Component({
  selector: 'app-cart-page',
  templateUrl: './cart-page.component.html',
  styleUrls: ['./cart-page.component.scss']
})
export class CartPageComponent implements OnInit {

  cardProduct: Product[] = []
  totalPrice = 0
  form: FormGroup
  submitted = false
  added = ''

  constructor(public productService: ProductService, private orderService: OrderService) { }

  ngOnInit(): void {
    this.cardProduct = this.productService.cardProduct
    for(let i = 0; i < this.cardProduct.length; i++) {
      this.totalPrice += +this.cardProduct[i].price
    }

    this.form = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      phone: new FormControl(null, [Validators.required]),
      address: new FormControl(null, [Validators.required]),
      payment: new FormControl('Cash')
    })
  }

  onSubmit() {
    if(this.form.invalid) {
      return;
    }

    this.submitted = true;

    const order = {
      name: this.form.value.name,
      phone: this.form.value.phone,
      address: this.form.value.address,
      orders: this.cardProduct,
      payment: this.form.value.payment,
      price: this.totalPrice,
      date: new Date()
    }

    this.orderService.create(order).subscribe(res => {
      this.form.reset()
      this.added = 'Delivery is framed'
      this.submitted = false
    })
  }

  delete(product) {
    this.totalPrice -= +product.price
    this.cardProduct.splice(this.cardProduct.indexOf(product), 1)
  }
}
